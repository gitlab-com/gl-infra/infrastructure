{ lib, pkgs, config, ... }:
with lib;
  let
    cfg = config.services;
    drivestrike  = import ./drivestrike.nix { pkgs = pkgs; };
  in {
    options.services = {
      drivestrike = {
        enable = mkEnableOption "DriveStrike Service";
      };
    };
    config = mkIf cfg.drivestrike.enable {
      # Calliope\ We'll add these to the path 
      # so the user can register.
      environment.systemPackages = with pkgs; [
        drivestrike
      ];
      systemd.services.drivestrike = {
        enable = true;
        description = "DriveStrike Client Service";
        path = [
          pkgs.dmidecode
          pkgs.glib-networking
          drivestrike
        ];
        unitConfig = {
          Description = "DriveStrike Client Service";
          After = [
            "network.target"
            "drivestrike-lock.service"
          ];
        };
        serviceConfig = {
          Type = "simple";
          Restart = "always";
          RestartSec = "10";
          ExecStart = "${drivestrike}/bin/drivestrike-start";
          SyslogIdentifier = "drivestrike";
          Environment = [
            "GITLAB_EMAIL=${config.gitlab.email}"
          ];
        };
        wantedBy = [ "multi-user.target" ];
      };
    };
  }


